export const apiEndPoints = {
  CITIES_LIST: '/group',
  CITY_WEATHER_FORECAST: '/forecast',
};

export const routeConstants = {
  HOME: 'home',
  DETAILS_WITH_ID: 'details/:cid',
  SETTINGS: 'settings'
};

export const cities = [
  {
    name: 'Moscow',
    id: 524901
  },
  {
    name: 'Kyiv',
    id: 703448
  },
  {
    name: 'London',
    id: 2643743
  },
  {
    name: 'Londesborough',
    id: 2643745
  },
  {
    name: 'Driffield',
    id: 2648120
  },
  {
    name: 'Magog',
    id: 6064180
  },
  {
    name: 'Maple Ridge',
    id: 6065686
  },
  {
    name: 'Mascouche',
    id: 6067494
  },
  {
    name: 'Medicine Hat',
    id: 6071618
  }

];


//https://api.openweathermap.org/data/2.5/group?id=524901,703448,2643743&units=metric&appid=3d8b309701a13f65b660fa2c64cdc517
