import {Component, OnInit} from '@angular/core';
import {WeatherService} from '@app/weather/services/weather.service';
import {cities} from '@app/constants/constants';
import { City, OpenWeatherCity} from '@app/weather/interfaces/city.interface';
import {Store} from "@ngrx/store";
import {Get_CITIES_WETHER} from "@app/weather/state/weather.action";
import {selectCitiesWeather} from "@app/weather/state/weather.selector";

@Component({
  selector: 'app-weather-dashboard',
  templateUrl: './weather-dashboard.component.html',
  styleUrls: ['./weather-dashboard.component.scss']
})

export class WeatherDashboardComponent implements OnInit {

  citiesList: City[] = cities;
  selCities !: number[];
  citiesTempList: OpenWeatherCity[] = [];

  constructor(private weatherService: WeatherService,
              private store: Store) {
   this.selCities = cities.slice(0, 5)?.map(ct => ct.id);
   this.store.select(selectCitiesWeather).subscribe((cities_weather: OpenWeatherCity[]) => {
     if (cities_weather) {
       this.citiesTempList = cities_weather;
     }
   });
  }

  ngOnInit(): void {
    this.getCitiesWithWeather();
  }

  getCitiesWithWeather(): void {
    // used Effects along with store to decrease the responsibility of the component for data fetch
    this.store.dispatch(Get_CITIES_WETHER({ cities : this.selCities }));
  }

  cityById(index: number, city: City): number {
   return city.id;
  }
}
