import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {WeatherService} from "@app/weather/services/weather.service";
import {OpenWeatherCity} from "@app/weather/interfaces/city.interface";
import {WeatherDetailsResponse} from "@app/weather/interfaces/weatherDetails.interface";
import {Store} from "@ngrx/store";
import {GET_WEATHER_FORECAST} from "@app/weather/state/weather.action";
import {selectCitiesWeather, selectWeatherForecast} from "@app/weather/state/weather.selector";

@Component({
  selector: 'app-weather-details',
  templateUrl: './weather-details.component.html',
  styleUrls: ['./weather-details.component.scss']
})
export class WeatherDetailsComponent implements OnInit {

  cityID!: string;
  city !: OpenWeatherCity;
  weatherForeCastList: OpenWeatherCity[] = [];
  timesForForeCast: number[] = [0, 3, 6, 9, 12, 15, 18, 21];
  timeForForeCast!: number;

  constructor(private actRoute: ActivatedRoute,
              private weatherService: WeatherService,
              private store: Store) {
    this.timeForForeCast = this.timesForForeCast[3];
    this.store.select(selectWeatherForecast).subscribe((weather_foreCast: OpenWeatherCity[]) => {
      if (weather_foreCast) {
        this.weatherForeCastList = weather_foreCast;
      }
    });
  }

  ngOnInit(): void {
    this.cityID = this.actRoute.snapshot.paramMap.get('cid') as string;
    if (this.cityID) {
      this.getWeatherDetailsOFCity();
    }
  }

  getWeatherDetailsOFCity(): void {
    this.weatherForeCastList = [];
    this.store.dispatch(GET_WEATHER_FORECAST({key: this.cityID}));
  }

  getWeatherIcon(dateObj: OpenWeatherCity): string {
   return this.weatherService.getWeatherIcon(dateObj);
  }

}
