import {Component, Input, OnInit} from '@angular/core';
import {OpenWeatherCity} from "@app/weather/interfaces/city.interface";
import {WeatherService} from "@app/weather/services/weather.service";

@Component({
  selector: 'app-city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.scss']
})
export class CityComponent implements OnInit {

  @Input() city!: OpenWeatherCity;

  constructor(private weatherService: WeatherService) { }

  ngOnInit(): void {
  }

  getWeatherIcon(city: OpenWeatherCity): string {
    return this.weatherService.getWeatherIcon(city);
  }
}
