import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'dateAtHour'
})
export class DateAtHourPipe implements PipeTransform {

  transform(dates: any[], timeForForeCast: number, ...args: any): any[] {
    return this.filterDatesAtNine(dates, timeForForeCast);
  }

  filterDatesAtNine(dates: any, timeForForeCast: number) {
    return dates.filter((dateEl: any) => {
      const date = new Date(dateEl?.dt_txt);
      return (date.getHours() === timeForForeCast);
    });
  }

}
