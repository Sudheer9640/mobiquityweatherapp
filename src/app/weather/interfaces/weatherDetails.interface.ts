export interface WeatherDetailsResponse {
  city: {
    id: number;
    name: string;
    main: {
      temp: number;
      sea_level: number;
    },
    sys: {
      sunrise: number;
      sunset: number;
      timezone: number;
    },
    weather: [{
      icon: string;
    }]
  };
  list: [{
    id: number;
    name: string;
    main: {
      temp: number;
      sea_level: number;
    },
    sys: {
      sunrise: number;
      sunset: number;
      timezone: number;
    },
    weather: [{
      icon: string;
    }]
  }]
}


