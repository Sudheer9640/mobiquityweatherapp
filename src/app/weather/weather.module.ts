import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {SharedModule} from '@shared/shared.module';
import {WeatherDashboardComponent} from './components/weather-dashboard/weather-dashboard.component';
import {WeatherDetailsComponent} from './components/weather-details/weather-details.component';
import { DateAtHourPipe } from './pipes/date-at-hour.pipe';
import { CityComponent } from './components/city/city.component';
import {EffectsModule} from "@ngrx/effects";
import {weatherFeatureKey, weathersReducer} from "@app/weather/state/weather.reducer";
import {WeatherEffects} from "@app/weather/state/weather.effects";
import {StoreModule} from "@ngrx/store";

@NgModule({
  declarations: [
    WeatherDashboardComponent,
    WeatherDetailsComponent,
    DateAtHourPipe,
    CityComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    EffectsModule.forFeature( [WeatherEffects]),
    StoreModule.forFeature(weatherFeatureKey, weathersReducer)
  ]
})
export class WeatherModule {
}
