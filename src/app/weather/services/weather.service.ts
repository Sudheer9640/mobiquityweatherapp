import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpParams} from '@angular/common/http';
import {HttpService} from '@core/services/http/http.service';
import {environment} from '@environments/environment';
import {apiEndPoints} from '@app/constants/constants';
import {OpenWeatherCity} from "@app/weather/interfaces/city.interface";

@Injectable({
  providedIn: 'root'
})

export class WeatherService {

  constructor(private httpService: HttpService) {
  }

  getWeathersOfCities(cities: number[]): Observable<any> {
    const citiesString = cities.join(',');
    let params = new HttpParams();
    params = params.append('id', citiesString);
    params = params.append('units', 'metric');
    return this.httpService.get(environment.OPEN_WEATHER_API + apiEndPoints.CITIES_LIST, params);
  }

  getWeatherForeCastOfCity(cityId: string): Observable<any> {
    let params = new HttpParams();
    params = params.append('id', cityId);
    params = params.append('units', 'metric');
    return this.httpService.get(environment.OPEN_WEATHER_API + apiEndPoints.CITY_WEATHER_FORECAST, params);
  }

  getWeatherIcon(city: OpenWeatherCity): string {
    const icon = city.weather[0]?.icon;
    return `http://openweathermap.org/img/w/${icon}.png`;
  }

}
