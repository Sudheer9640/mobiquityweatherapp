import {Action, createReducer, on} from '@ngrx/store';
import {GET_CITIES_WEATHER_SUCCESS, GET_WEATHER_FORECAST_SUCCESS} from './weather.action';
import {initialState, WeatherState} from './weather.state';

export const weatherFeatureKey = 'weather';

const weatherReducer = createReducer(
  initialState,
  on(GET_CITIES_WEATHER_SUCCESS, (state, {citiesWeather}) =>
    ({
      ...state,
      citiesWeather
    })),
  on(GET_WEATHER_FORECAST_SUCCESS, (state, {forecastWeather, forecastCity}) => ({
    ...state, forecastWeather, forecastCity
  })),
);

export const weathersReducer = (state: WeatherState, action: Action) =>
  weatherReducer(state, action);
