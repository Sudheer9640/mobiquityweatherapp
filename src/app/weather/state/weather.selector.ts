import { createFeatureSelector, createSelector } from '@ngrx/store';
import { WeatherState } from './weather.state';
import { weatherFeatureKey } from './weather.reducer';

export const selectWeatherState = createFeatureSelector<WeatherState>(
  weatherFeatureKey
);

export const selectCitiesWeather = createSelector(
  selectWeatherState,
  (state: WeatherState) => state.citiesWeather
);

export const selectWeatherForecast = createSelector(
  selectWeatherState,
  (state: WeatherState) => state.forecastWeather
);
