import {Injectable} from '@angular/core';
import {Actions, createEffect, ofType} from '@ngrx/effects';
import {EMPTY} from 'rxjs';
import {catchError, map, mergeMap} from 'rxjs/operators';
import {WeatherService} from '@app/weather/services/weather.service';
import {
  GET_CITIES_WEATHER_SUCCESS,
  Get_CITIES_WETHER,
  GET_WEATHER_FORECAST,
  GET_WEATHER_FORECAST_SUCCESS
} from './weather.action';
import {CitiesWeatherResponse} from "@app/weather/interfaces/citiesWeather.interface";
import {WeatherDetailsResponse} from "@app/weather/interfaces/weatherDetails.interface";

@Injectable()
export class WeatherEffects {
  constructor(private actions$: Actions, private weatherService: WeatherService) {
  }

  getCitiesWeather = createEffect(() =>
    this.actions$.pipe(
      ofType(Get_CITIES_WETHER),
      mergeMap((action) =>
        this.weatherService.getWeathersOfCities(action.cities).pipe(
          map(
            (citiesWeatherRes: CitiesWeatherResponse) =>
              GET_CITIES_WEATHER_SUCCESS({
                citiesWeather: citiesWeatherRes.list
              }),
            catchError(() => EMPTY)
          )
        )
      )
    )
  );

  getWeatherForeCast = createEffect(() =>
    this.actions$.pipe(
      ofType(GET_WEATHER_FORECAST),
      mergeMap((action) =>
        this.weatherService.getWeatherForeCastOfCity(action.key).pipe(
          map((forecastRes: WeatherDetailsResponse) => {
              const {list, city} = forecastRes;
              if (list && city && Array.isArray(list)) {
                return GET_WEATHER_FORECAST_SUCCESS({forecastWeather: list, forecastCity: city});
              } else {
                throw new Error('Invalid response');
              }
            }
          ),
          catchError(() => EMPTY)
        )
      )
    )
  );
}
