
import { createAction, props } from '@ngrx/store';

export const Get_CITIES_WETHER = createAction(
  '[CITIES_WEATHER] - Get Cities Weather',
  props<{ cities: number[] }>()
);

export const GET_CITIES_WEATHER_SUCCESS = createAction(
  '[CITIES_WEATHER_SUCCESS] - Get Cities Weather Success',
  props<{citiesWeather: any[] }>()
);

export const GET_WEATHER_FORECAST = createAction(
  '[WEATHER_FORECAST] - Get City Weather Forecast',
  props<{ key: string }>()
);

export const GET_WEATHER_FORECAST_SUCCESS = createAction(
  '[CITIES_WEATHER_SUCCESS] - Get Cities Forecast Success',
  props<{forecastWeather: any[], forecastCity: any }>()
);
