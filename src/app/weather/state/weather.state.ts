import {City, OpenWeatherCity} from "@app/weather/interfaces/city.interface";

export interface WeatherState {
  citiesWeather: OpenWeatherCity[],
  forecastWeather: OpenWeatherCity[],
  forecastCity: City
}

export const initialState: WeatherState = {
  citiesWeather:  [],
  forecastWeather: [],
  forecastCity: {
    name: '',
    id: 0
  }
};
