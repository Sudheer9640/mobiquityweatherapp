import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MaterialModule} from '@shared/material/material.module';
import {RouterModule} from '@angular/router';
import {FormsModule} from '@angular/forms';

const modules = [MaterialModule, RouterModule, FormsModule];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ...modules
  ],
  exports: [ ...modules ]
})
export class SharedModule { }
//we use this to share commonly used components or pipes  across feature modules
