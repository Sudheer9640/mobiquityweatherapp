import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PageNotFoundComponent} from './page-not-found/page-not-found.component';
import {LayoutComponent} from '@core/components/layout/layout.component';
import {WeatherDashboardComponent} from '@app/weather/components/weather-dashboard/weather-dashboard.component';
import {WeatherDetailsComponent} from '@app/weather/components/weather-details/weather-details.component';
import {SettingsComponent} from '@core/components/settings/settings.component';
import {routeConstants} from "@app/constants/constants";

const routes: Routes = [
  { path: '', component: LayoutComponent, children: [
      { path: '', redirectTo: '/home', pathMatch: 'full'},
      {path: routeConstants.HOME, component: WeatherDashboardComponent},
      {path: routeConstants.DETAILS_WITH_ID, component: WeatherDetailsComponent},
      {path: routeConstants.SETTINGS, component: SettingsComponent}
    ]
  },
  { path: '**', component: PageNotFoundComponent },
];

// we specify wild card route if page not found
// we can load feature modules using lazy loading to reduce initial load of app.


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
