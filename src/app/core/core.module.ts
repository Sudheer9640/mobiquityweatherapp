import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {SharedModule} from '@shared/shared.module';
import { AlertComponent } from "@core/components/alert/alert.component";
import { SpinnerComponent } from "@core/components/spinner/spinner.component";
import {HeaderComponent} from "@core/components/header/header.component";
import {SideNavComponent} from "@core/components/side-nav/side-nav.component";
import {LayoutComponent} from "@core/components/layout/layout.component";
import {SettingsComponent} from "@core/components/settings/settings.component";

@NgModule({
  declarations: [
    AlertComponent,
    HeaderComponent,
    SideNavComponent,
    SpinnerComponent,
    LayoutComponent,
    SettingsComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    AlertComponent,
    HeaderComponent,
    SideNavComponent,
    SpinnerComponent
  ],
})
export class CoreModule { }

// we use this module which deals with the functionality which is common across all feature modules.
// And it needs to be imported only in app module
