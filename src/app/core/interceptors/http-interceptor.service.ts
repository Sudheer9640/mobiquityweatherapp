import {Injectable} from '@angular/core';
import {
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpParams,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';
import {AlertService} from '@core/services/alert/alert.service';
import {SpinnerService} from '@core/services/spinner/spinner.service';
import {environment} from "@environments/environment";

@Injectable()

export class HttpInterceptorService implements HttpInterceptor {

  constructor(private alertService: AlertService,
              private spinner: SpinnerService
  ) {}

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.spinner.display(true);
    const req = request.clone({
      params: (request.params ? request.params : new HttpParams())
        .set('appid',environment.OPEN_WEATHER_APP_ID)
    });
    const started = Date.now();
    return next.handle(req).pipe(
      tap(event => {
        if (event instanceof HttpResponse) {
          this.spinner.display(false);
          const elapsed = Date.now() - started;
          console.log(`Request for ${req.urlWithParams} took ${elapsed} ms.`);
        }
      }, (error) => {
        this.spinner.display(false);
        console.error('ERROR', error, error.error?.userMessage);
          this.alertService.error(error.statusText);
      }));
  }

}
