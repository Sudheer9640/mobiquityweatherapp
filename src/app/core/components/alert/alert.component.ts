import { Component, OnInit, Inject, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { AlertService } from '@core/services/alert/alert.service';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})

export class AlertComponent implements OnInit, OnDestroy {
  private subscription!: Subscription;
  message: any;

  constructor(private alertService: AlertService,
              public dialog: MatDialog) { }

  ngOnInit(): void {
    this.subscription = this.alertService.getMessage().subscribe((message: any) => {
      if (message) {
        this.message = message;
      }
    });
  }

  dismissAlert(): void {
    this.message = '';
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
