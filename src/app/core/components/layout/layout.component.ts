import {ChangeDetectorRef, Component, OnDestroy} from '@angular/core';
import {MediaMatcher} from "@angular/cdk/layout";

@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.scss']
})
export class LayoutComponent implements OnDestroy {

  mobileQuery!: MediaQueryList;
  showLabel!: boolean;
  private mobileQueryListener: any;

  constructor(private media: MediaMatcher,
              private changeDetectorRef: ChangeDetectorRef) {
    this.mobileQuery = media.matchMedia('(max-width: 600px)');
    this.mobileQueryListener = () => changeDetectorRef.detectChanges();
    this.mobileQuery.addEventListener(this.mobileQueryListener, () => {});
  }

  ngOnDestroy(): void {
    this.mobileQuery.removeEventListener(this.mobileQueryListener, () => {
    });
  }

}
