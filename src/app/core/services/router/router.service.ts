import {Injectable} from '@angular/core';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class RouterService {

  constructor(private router: Router) {
  }

  navigate(route: string, extras?: any): void {
    this.router.navigate([route], extras);
  }
}
