# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Task 

### How do I get set up? ###

* Need to install node and angular latest version
* Need to run npm install
* Need to build the app using ng build 
* Can you run app using ng serve

### In home page, User can select multiple cities using dropdown

### Selected cities with weather details will be shown.

### On clicking particular city tile, city details with weather forecast for next 5 days will be shown in details page.

### Using can select particular time using dropdown for next five days for which forecast to be shown.



### App splitted into core, shared and feature modules to reduce initial load weight of app.

### Used ngrx store for state management and ngrx effects

### Developed responsive ui using css since its light weight


Adding screenshots of App.

[![Screenshot-235.png](https://i.postimg.cc/Vv0fpXmP/Screenshot-235.png)](https://postimg.cc/V01PbrJK)

[![Screenshot-236.png](https://i.postimg.cc/WtRNvt1F/Screenshot-236.png)](https://postimg.cc/SYrFLS8k)

[![Screenshot-237.png](https://i.postimg.cc/mD5ZmmxS/Screenshot-237.png)](https://postimg.cc/xJLSCy1k)

[![Screenshot-238.png](https://i.postimg.cc/sfM5s82s/Screenshot-238.png)](https://postimg.cc/9zjDBLTn)

[![Screenshot-241.png](https://i.postimg.cc/DwrZ7Qs2/Screenshot-241.png)](https://postimg.cc/hXGgrdpw)

[![Screenshot-244.png](https://i.postimg.cc/CKwK5ZHb/Screenshot-244.png)](https://postimg.cc/RWbmbZY0)

